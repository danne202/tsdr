﻿using System.Collections.Generic;
using System.Linq;

namespace RestAPITimebooking.Helpers
{
    public class DatabaseHelper
    {

        /// <summary>
        /// Finds user by ID from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static UserMaster GetUserById(int id)
        {
            using (var context = new UserMasterContext())
            {
                return context.UserMaster.FirstOrDefault(x => x.UserId == id);
            }
        }

        /// <summary>
        /// Gets all bookings in the database 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userRole"></param>
        /// <returns>If the user is authorized as admin, it returns all the information about the bookings.
        /// Else it just returns the start and end-date</returns>
        public static List<Booking> GetAllBookings(int userId, string userRole)
        {
            using (var context = new BookingsContext())
            {
                var dataList = (from b in context.Bookings
                                join u in context.UserMasters on b.userId equals u.UserId
                                select new
                                {
                                    StartDate = b.start_date,
                                    EndDate = b.end_date,
                                    Id = b.id,
                                    UserId = b.userId,
                                    text = u.Username 
                                }).ToList()
                                .Select(b => new Booking
                                {
                                    start_date = b.StartDate,
                                    end_date = b.EndDate, 
                                    id = b.Id,
                                    userId = b.UserId,
                                    text = b.text }).ToList();

                if(userRole == "Admin")
                {
                    return dataList;
                }
                else
                {
                    foreach (var item in dataList)
                    {
                        if (item.userId != userId)
                        {
                            item.text = "";
                        }
                    }
                    return dataList;
                }
            }
        }

        /// <summary>
        /// Returns a booking with {id} from the API in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Booking GetBookingById(int id)
        {
            using (var context = new BookingsContext())
            {
                return context.Bookings.FirstOrDefault(x => x.id == id);
            }
        }

        /// <summary>
        /// Adds new user entry in database. Happens when a user registers on the API
        /// </summary>
        /// <param name="user"></param>
        public static void AddUserToDb(UserMaster user)
        {
            using (var context = new UserMasterContext())
            {
                context.UserMaster.Add(user);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Creates a new booking if there's no booking with the same start-date.
        /// </summary>
        /// <param name="booking"></param>
        public static void AddNewBooking(Booking booking)
        {
            using (var context = new BookingsContext())
            {
                //If a booking is already occupying this when creating, don't create a new one.
                if(context.Bookings.Any(x => x.start_date == booking.start_date))
                {
                    return;
                }
                context.Bookings.Add(booking);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Get's booking by ID from API and deletes it. This method can only be called if the user is authorized.
        /// </summary>
        /// <param name="id"></param>
        public static void DeleteBookingByid(int id)
        {
            using (var context = new BookingsContext())
            {
                Booking bookingToRemove = context.Bookings.Where(x => x.id == id).FirstOrDefault();
                context.Bookings.Remove(bookingToRemove);
                context.SaveChanges();
            }
        }
    }
}