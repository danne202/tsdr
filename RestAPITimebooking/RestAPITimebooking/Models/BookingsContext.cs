namespace RestAPITimebooking
{
    using System.Data.Entity;

    public partial class BookingsContext : DbContext
    {
        public BookingsContext()
            : base("name=BookingsContext")
        {
        }

        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<UserMaster> UserMasters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
