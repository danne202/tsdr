namespace RestAPITimebooking
{
    using System;

    public partial class Booking
    {
        public int id { get; set; }
        public int? userId { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string text { get; set; }
    }
}




