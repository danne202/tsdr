﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPITimebooking.Models
{
    public class UserRepository : IDisposable
    {
        UserMasterContext context = new UserMasterContext();

        public UserMaster ValidateUser(string username, string password)
        {
            return context.UserMaster.FirstOrDefault(user =>
            user.Username.Equals(username, StringComparison.OrdinalIgnoreCase)
            && user.Password == password);
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}