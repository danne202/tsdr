namespace RestAPITimebooking
{
    using System.Data.Entity;

    public partial class UserMasterContext : DbContext
    {
        public UserMasterContext()
            : base("name=UserMasterContext")
        {
        }

        public virtual DbSet<UserMaster> UserMaster { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserMaster>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<UserMaster>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<UserMaster>()
                .Property(e => e.Role)
                .IsUnicode(false);

            modelBuilder.Entity<UserMaster>()
                .Property(e => e.EmailId)
                .IsUnicode(false);
        }
    }
}
