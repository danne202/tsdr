﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using RestAPITimebooking.Helpers;

namespace RestAPITimebooking
{

    public class UserMasterController : ApiController
    {

        [Authorize]
        public UserMaster Get()
        {
            var identity = (ClaimsIdentity)User.Identity;

            var id = int.Parse(identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                   .Select(c => c.Value).FirstOrDefault());

            var user = DatabaseHelper.GetUserById(id);

            return user;
        }

        [Authorize]
        public UserMaster Get(int id)
        {           
            return DatabaseHelper.GetUserById(id);
        }

        [AllowAnonymous]
        public void Post([FromBody]UserMaster value)
        {

            UserMaster user = new UserMaster()
            {
                Username = value.Username,
                Password = value.Password,
                EmailId = value.EmailId,
                Role = "User"
            };

            DatabaseHelper.AddUserToDb(user);

        }

        [Authorize]
        public void Put(int id, [FromBody]string value)
        {
        }

        [Authorize]
        public void Delete(int id)
        {
        }
    }
}