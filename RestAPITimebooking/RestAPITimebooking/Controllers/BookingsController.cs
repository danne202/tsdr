﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RestAPITimebooking.Helpers;
using System.Security.Claims;

namespace RestAPITimebooking.Controllers
{

    public class BookingsController : ApiController
    {

        [Authorize]
        public IEnumerable<Booking> Get()
        {
            var identity = (ClaimsIdentity)User.Identity;

            var id = int.Parse(identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                   .Select(c => c.Value).FirstOrDefault());

            string userRole = identity.Claims.Where(x => x.Type == ClaimTypes.Role)
                .Select(c => c.Value).FirstOrDefault().ToString();

            return DatabaseHelper.GetAllBookings(id, userRole);
        }

        [Authorize]
        public Booking Get(int id)
        {
            return DatabaseHelper.GetBookingById(id);
        }

        [Authorize]
        public void Post([FromBody]Booking value)
        {

            var identity = (ClaimsIdentity)User.Identity;

            var id = int.Parse(identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                   .Select(c => c.Value).FirstOrDefault());

            Booking booking = new Booking()
            {
                userId = id,
                start_date = value.start_date,
                end_date = value.end_date,
                text = value.text
            };

            DatabaseHelper.AddNewBooking(booking);
        }

        [Authorize]
        public void Put(int id, [FromBody]string value)
        {
        }

        [Authorize]
        public void Delete(int id)
        {
            DatabaseHelper.DeleteBookingByid(id);
        }
    }
}