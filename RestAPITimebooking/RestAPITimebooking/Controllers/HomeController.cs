﻿using System.Web.Mvc;

namespace RestAPITimebooking.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "TSDR API";

            return View();
        }
    }
}
