﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using TimeBookingWeb.Models;

namespace TimeBookingWeb.API
{
    public class UserAPI
    {
        public static UserModel GetUserById(int id)
        {

            HttpWebRequest apiRequest =
            WebRequest.Create("https://tsdrapi.azurewebsites.net/api/usermaster/"+id.ToString()) as HttpWebRequest;

            string apiResponse = "";
            using (HttpWebResponse response = apiRequest.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                apiResponse = reader.ReadToEnd();
            }

            UserModel userModelObject = JsonConvert.DeserializeObject<UserModel>(apiResponse);

            return userModelObject;
        }

        public static IEnumerable<BookingModel> GetAllBookings(UserModel user)
        {
           HttpWebRequest apiRequest =
           WebRequest.Create("https://tsdrapi.azurewebsites.net/api/bookings") as HttpWebRequest;

            string apiResponse = "";
            using (HttpWebResponse response = apiRequest.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                apiResponse = reader.ReadToEnd();
            }

            List<BookingModel> modelObject = JsonConvert.DeserializeObject<List<BookingModel>>(apiResponse);

            return modelObject;
        }
    }
}