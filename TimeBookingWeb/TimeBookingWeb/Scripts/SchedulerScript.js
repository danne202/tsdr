﻿var format = scheduler.date.date_to_str("%H:%i");
var step = 30;

scheduler.templates.hour_scale = function (date) {
    var html = "";
    for (var i = 0; i < 60 / step; i++) {
        html += "<div style='height:22px;line-height:22px;'>" + format(date) + "</div>";
        date = scheduler.date.add(date, step, "minute");
    }
    return html;
}

//var dp = new dataProcessor("https://tsdrapi.azurewebsites.net/token");
//dp.init(scheduler);
//dp