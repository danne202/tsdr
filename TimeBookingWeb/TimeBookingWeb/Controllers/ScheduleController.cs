﻿using DHTMLX.Common;
using DHTMLX.Scheduler;
using DHTMLX.Scheduler.Data;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TimeBookingWeb.Models;
using DHTMLX.Scheduler.Authentication;

namespace TimeBookingWeb.Controllers
{   
    public class ScheduleController : Controller
    {
        public static string token { get; set; }

        public static UserModel currentLoggedInUser { get; set; }

        

        public static int currentUserId { get; set; }
        public static string currentUserRole { get; set; }

        System.Web.HttpCookie tokenCookie = new System.Web.HttpCookie("token");
        string tokenString = string.Empty;

        // GET: Schedule
        public ActionResult Index(string value)
        {

            currentLoggedInUser = GetCurrentLoggedOnUser();

            currentUserId = currentLoggedInUser.UserId;
            currentUserRole = currentLoggedInUser.Role;

            var scheduler = new DHXScheduler(this);

            scheduler.EnableDataprocessor = true;
            scheduler.Views.Items.RemoveAt(2);
            scheduler.Config.drag_create = false;
            scheduler.Config.event_duration = 30;
            scheduler.Config.drag_resize = false;
            scheduler.Config.drag_move = false;
            scheduler.Config.drag_create = false;

            scheduler.LoadData = true;
            scheduler.EnableDataprocessor = true;

            SchedulerUser user = new SchedulerUser();
            user.UserIdKey = currentLoggedInUser.UserId.ToString();
            user.EventUserIdKey = currentLoggedInUser.UserId.ToString();
            scheduler.Authentication = user;
            scheduler.SetUserDetails(user, user.UserIdKey, user.EventUserIdKey);

            scheduler.SetEditMode(EditModes.OwnEventsOnly);

            scheduler.UpdateFieldsAfterSave();

            return View(scheduler);
        }
        public ActionResult Interm(string value)
        {

            tokenCookie["token"] = value;
            Response.Cookies.Add(tokenCookie);

            return RedirectToAction("Index");
        }
        public ActionResult Logout()
        {

            tokenCookie = Request.Cookies["token"];

            if (tokenCookie != null)
                tokenCookie.Expires = DateTime.Now.AddDays(-1);

            Response.Cookies.Add(tokenCookie);

            return RedirectToAction("Login", "Home");
        }
        public ContentResult Data()
        {
            //var Token = value;
            HttpWebRequest apiRequest =
            WebRequest.Create("https://tsdrapi.azurewebsites.net/api/bookings") as HttpWebRequest;          
            string apiResponse = "";

            tokenCookie = Request.Cookies["token"];

            if (tokenCookie != null)
                tokenString = tokenCookie.Value.Split('=')[1];

            apiRequest.Headers.Add("Authorization", "Bearer " + tokenString);
            using (HttpWebResponse response = apiRequest.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                apiResponse = reader.ReadToEnd();
            }

            var rootObject = JsonConvert.DeserializeObject<IEnumerable<BookingModel>>(apiResponse);

            var data = new SchedulerAjaxData(rootObject);

            return (ContentResult)data;
        }

        public ContentResult Save(int? id, FormCollection actionValues)
        {
            var action = new DataAction(actionValues);
            var changedEvent = (BookingModel)DHXEventsHelper.Bind(typeof(BookingModel), actionValues);
            try
            {
                switch (action.Type)
                {
                    case DataActionTypes.Insert:
                        PostData(changedEvent);
                        break;
                    case DataActionTypes.Delete:

                        if(currentLoggedInUser.Role.Equals("Admin") || currentLoggedInUser.UserId.Equals(changedEvent.userId))
                        {
                            DeleteData(changedEvent);
                        } 
                        else {
                            action.Message = "Can't delete this!";
                            changedEvent = null;
                            break;
                        }
                        break;
                    default:// "update" // define here your Update logic
                        break;
                }
            }
            catch
            {
                action.Type = DataActionTypes.Error;
            }
            
            return (new AjaxSaveResponse(action));
        }

        public void PostData(BookingModel bookingModel)
        {

            tokenCookie = Request.Cookies["token"];

            if (tokenCookie != null)
                tokenString = tokenCookie.Value.Split('=')[1];

            var client = new RestClient("https://tsdrapi.azurewebsites.net/api/bookings");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + tokenString);
            request.AddParameter("userId", currentLoggedInUser.UserId);
            request.AddParameter("start_date", bookingModel.start_date);
            request.AddParameter("end_date", bookingModel.end_date);
            request.AddParameter("text", currentLoggedInUser.Username);
            IRestResponse response = client.Execute(request);
        }

        public void DeleteData(BookingModel bookingModel)
        {

            var client = new RestClient("https://tsdrapi.azurewebsites.net/api/bookings/" + bookingModel.id);
            var request = new RestRequest(Method.DELETE);

            tokenCookie = Request.Cookies["token"];

            if (tokenCookie != null)
                tokenString = tokenCookie.Value.Split('=')[1];

            request.AddHeader("Authorization", "Bearer " + tokenString);
            IRestResponse response = client.Execute(request);
        }

        private UserModel GetCurrentLoggedOnUser()
        {
            HttpWebRequest apiRequest =
            WebRequest.Create("https://tsdrapi.azurewebsites.net/api/usermaster") as HttpWebRequest;

            tokenCookie = Request.Cookies["token"];

            if(tokenCookie != null)
                tokenString = tokenCookie.Value.Split('=')[1];

            string apiResponse = "";
            apiRequest.Headers.Add("Authorization", "Bearer " + tokenString);
            using (HttpWebResponse response = apiRequest.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                apiResponse = reader.ReadToEnd();
            }

            var user = JsonConvert.DeserializeObject<UserModel>(apiResponse);

            return user;
        }
    }
}